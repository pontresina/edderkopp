# -*- coding: utf-8 -*-
"""edderkopp.app

Module containing the core spider
"""
from __future__ import print_function

import contextlib
import functools
import logging
import pprint
import Queue
import re
import os
import requests
import time
import networkx as nx

from edderkopp import metadata, utils

from reppy.cache import RobotsCache
from reppy.exceptions import ServerError
from requests.exceptions import RequestException
from requests.structures import CaseInsensitiveDict
from bs4 import BeautifulSoup as BS
from urlparse import urljoin
from collections import namedtuple
from tempfile import gettempdir

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

useragent = "{0}/{1}".format(metadata.project, metadata.version)


class FetchError(Exception):
    """ Umbrella exception class for errors within URLFetcher """
    def __init__(self, msg, url):
        Exception.__init__(self, msg)
        self.url = url


def _RobotsCacheFactory(**kwargs):
    return RobotsCache(**kwargs)


class Robots(object):
    """ Defaults to 2 seconds between each HTTP request """
    def __init__(self, headers, url, rc=_RobotsCacheFactory):
        self.url = url  # before any HTTP redirects
        self.delay = 2.0
        self.headers = headers
        if not 'User-Agent' in self.headers:
            raise ValueError('headers must contain a User-Agent')
        self.useragent = self.headers.get('User-Agent')
        self.robots = rc(headers=self.headers)

    def download_robots_txt(self):
        self.robots.cache(self.url)
        robot_delay = self.robots.delay(self.url, self.useragent)
        if robot_delay is not None:
            self.delay = robot_delay

    def allowed(self, url):
        return self.robots.allowed(url, self.useragent)

    def clear(self):
        self.robots.clear()

    def __repr__(self):
        return '<Robots [delay={0}, url={1}, useragent={2}]>'.format(
               self.delay, self.url, self.useragent)


class EventSubscriber(object):
    """ tracks progress of the Crawler by generating
        events through these callback methods.
    """
    def __init__(self):
        pass

    def enqueued(self, url, first=False):
        """ time when the root url is added to the FIFO q """
        msg = "{0} enqueued".format(url) + (" (root)" if first else "")
        logger.info(msg)

    def denied(self, url):
        """ invoked when url is denied by robots.txt """
        logger.debug("%s disallowed from crawlers", url)

    def fetch_error(self, exc, url):
        """ fetching `url` encountered an IOError """
        logger.debug("FetchError exception for %s", url)

    def problem(self, reason, url):
        logger.debug("%s -> %s", reason, url)


class Crawler(object):
    """ Encapsulates the lifecycle for crawling.

        Collaborators:
        * robots.txt cache
    """
    headers = {'User-Agent': useragent}
    ASSET_KEYS = ['js', 'css', 'img', 'vid']

    def __init__(self, url, rbt=Robots, sleep=time.sleep,
                 q=Queue.Queue, subscriber=EventSubscriber,
                 dg=nx.DiGraph):
        self.url = url
        self.snooze = sleep
        self.fifo_q = q()
        self.robot = rbt(self.headers, self.url)
        self.subscriber = subscriber()
        self.digraph = dg(name='sitemap')

    def add_node(self, node):
        if node not in self.digraph:
            kwargs = {}
            if self.digraph.number_of_nodes() == 0:
                kwargs.update({'root': True})
            self.digraph.add_node(node, **kwargs)

    def add_assets(self, parent, results):
        for key in self.ASSET_KEYS:
            for asset in results['assets'][key]:
                if asset not in self.digraph:
                    self.digraph.add_node(asset)
                if not self.digraph.has_edge(parent, asset):
                    self.digraph.add_edge(parent, asset, relation=key)

    def open(self):
        """
        :raises: ``reppy.exceptions.ServerError`` if network error
        """
        logger.info('Crawler.open')
        self.robot.download_robots_txt()
        logger.info('\t..Downloaded robots.txt %s' % repr(self.robot))

    def crawl(self):
        """ breadth first search """
        logger.info('Crawler.crawl')
        try:
            count = 0
            explored = set([])  # urls that have been visited & its neighbours
                                # expanded should be added to this set.
            queued = set([])
            denied_by_robot = set([])

            # kickstart BFS
            self.fifo_q.put(self.url)
            queued.add(self.url)
            self.subscriber.enqueued(self.url, True)

            while not self.fifo_q.empty():
                count = count + 1
                logger.info("count=%d", count)
                #if count > 3:
                #    break
                a_url = self.fifo_q.get()
                logger.info(' .. dequeued {0}\n'.format(a_url))

                # are we allowed to fetch ?
                try:
                    if not self.robot.allowed(a_url):
                        logger.info('%s not allowed', a_url)
                        denied_by_robot.add(a_url)
                        self.subscriber.denied(a_url)
                        self.fifo_q.task_done()
                        continue
                except ServerError:
                    self.fifo_q.task_done()
                    continue

                try:
                    # explore ``a_url``
                    results = URLFetcher(self.headers).fetch(a_url)
                    explored.add(results['url'])  # redirected url ?
                    if a_url != results['url']:
                        explored.add(a_url)  # original != redirected

                    parent = (results['url']
                              if a_url != results['url'] else a_url)
                    #print('parent=', parent, "; a_url=", a_url)

                    if "reason" in results:
                        self.subscriber.problem(results['reason'], a_url)
                        self.fifo_q.task_done()
                        continue

                    self.add_node(parent)
                    self.add_assets(parent, results)

                    if "explore" in results:
                        candidates = results['explore']
                        for c in candidates:
                            self.digraph.add_edge(parent, c)
                            if c not in explored:
                                if c not in queued:
                                    self.fifo_q.put(c)
                                    queued.add(c)
                                    self.subscriber.enqueued(c)

                    self.fifo_q.task_done()

                except FetchError, fe:
                    self.subscriber.fetch_error(fe, a_url)
                    explored.add(a_url)
                    self.fifo_q.task_done()

                finally:
                    logger.info('sleeping')
                    self.snooze(self.robot.delay)

            #logger.info(explored)
            #logger.info(denied_by_robot)
            #print(repr(self.digraph.adj))
            filename = utils.dotfilename(self.url)
            fullpath = gettempdir() + os.sep + filename
            nx.write_dot(self.digraph, fullpath)
            print("Finished crawling.\n")
            print("\tYour generated graphiz file is in {0}\n".format(fullpath))
            print("\tTo export the sitemap to a PNG file, do:")
            print("\t\t$ dot {0} -Tpng -o sitemap.png".format(filename))
            return 0
        except KeyboardInterrupt:
            return 130

    def close(self):
        logger.info('Crawler.close')
        #print(self.digraph.adj)
        self.robot.clear()

    def __repr__(self):
        return '<Crawler [%s]>' % (self.url)


class URLFetcher(object):
    """ Responsibility: fetches the links.

        Collaborators:
        * Link extractors
        * Asset extractors
    """
    codes_400 = xrange(400, 419)
    codes_500 = xrange(500, 506)

    re_mimehtml = re.compile("^text/html.*")

    def __init__(self, headers):
        self.headers = headers
        self.pp = pprint.PrettyPrinter(indent=4, width=70)

    def fetch(self, url):
        """ This will follow redirects.
            :return: list of urls
            :raises: ``FetchError`` if network error prevented the execution.
            :raises: ``RuntimeError`` if unhandled edge conditions encountered.
        """
        try:
            response = requests.get(url, headers=self.headers,
                                    allow_redirects=True)
            # preconditions:
            # - any redirects 301, 302, 303, 307 were followed.
            if response.status_code == 200:
                ishtml, msg1 = self.is_html(response)
                if not ishtml:
                    logger.error(msg1)
                    return Responder.empty(response.url,
                                           tuple([901, msg1]))

                notempty, msg2 = self.not_empty(response)
                if not notempty:
                    logger.error(msg2)
                    return Responder.empty(response.url,
                                           tuple([903, u'No Content']))

                parser = Parser(response.text, response.url)
                with contextlib.closing(parser):
                    logger.info("parser.follow_links = %r",
                                parser.follow_links)
                    return parser.parse()

            elif response.status_code == 204:  # No Content
                    return Responder.empty(response.url,
                                           tuple([903, u'No Content']))

            elif ((response.status_code in self.codes_400) or
                 (response.status_code in self.codes_500)):
                logger.info('ignored [%s] due to HTTP response [%d]',
                            response.url, response.status_code)
                return Responder.empty(response.url,
                                       tuple([902,
                                              u'HTTP {0}'.format(
                                                  response.status_code)]))

            else:
                raise RuntimeError('unhandled status [%d] url [%s]' %
                                   (response.status_code, url))

        except RequestException, e:
            raise FetchError("get [%s] failed with [%s]" % (url, e), url)

    @classmethod
    def is_html(cls, response):
        actual_mime = response.headers.get('content-type', '')
        ishtml = cls.re_mimehtml.match(actual_mime.strip()) is not None
        return (ishtml, u"" if ishtml else u"{0}".format(actual_mime))

    @classmethod
    def not_empty(cls, response):
        notempty = len(response.text) > 0
        return (notempty, "" if notempty else "empty body")

    def __repr__(self):
        return '<URLFetcher [%d]>' % (id(self))


SuperAsset = namedtuple('Asset', 'name hash')


@functools.total_ordering
class Asset(SuperAsset):
    def __init__(self, *args):
        super(Asset, self).__init__(*args)

    def __hash__(self):
        return super(Asset, self).hash

    def __eq__(self, other):
        if type(other) != Asset:
            return NotImplemented
        return super(Asset, self).hash == other.hash

    def __lt__(self, other):
        if type(other) != type(self):
            return NotImplemented
        return super(Asset, self).hash < other.hash

    def __repr__(self):
        return 'Asset(%r, %r)' % (self.name, self.hash)


class Responder(object):
    """ Responsible for constructing responses from parsing URLs """

    keys = ['js', 'css', 'img', 'vid']

    @staticmethod
    def with_args_for_graph(a_response):
        """ Only retains the artifact name for each asset.
            As an example::

                u'https://www.digitalocean.com/asset/video.mp4'

            is turned into a ``namedtuple``:

                Asset(u'video.mp4', 302691733)

            where ``302691733`` is the ``hash`` of the original URL.

            :param a_response: a :class:`dict` as returned by
                               method ``Responder.with_args``
            :return: ``None``. It mutates argument ``a_response``.
        """
        if 'assets' in a_response:
            assets = a_response['assets']
            for key in Responder.keys:
                current = assets.get(key, frozenset([]))
                transformed = Responder._strip_prefix(current)
                assets.update({key: transformed})
            #pprint.pprint(assets)

    @staticmethod
    def _strip_prefix(a_set):
        if a_set:
            tmp = []
            for url in a_set:
                basename = url.rsplit('/', 1)
                if len(basename) == 2:
                    tmp.append(Asset(basename[1], hash(url)))
            return frozenset(tmp)
        else:
            return a_set

    @staticmethod
    def empty(final_url, reason=None):
        return Responder.with_args(final_url, reason=reason)

    @staticmethod
    def with_args(final_url, hrefs=None, js=None,
                  css=None, img=None, vids=None, reason=None):
        retval = CaseInsensitiveDict()
        if final_url:
            retval.update({'url': final_url})
        if hrefs is None:
            hrefs = []
        if js is None:
            js = frozenset([])
        if css is None:
            css = frozenset([])
        if img is None:
            img = frozenset([])
        if vids is None:
            vids = frozenset([])

        retval.update({'assets': {'js': js,
                                  'css': css,
                                  'img': img,
                                  'vid': vids}})
        retval.update({'explore': hrefs})
        if reason is not None:
            retval.update({'reason': reason})
        return retval


class Parser(object):
    """ Parses the html document:
        - Link extractors
        - Asset extractors
    """
    absolute_urls1_re = re.compile(r'^http[s]?:.*$', flags=re.I)
    relative_urls_re = re.compile(r"^//(?P<restofurl>.*)$", flags=re.I)

    def __init__(self, text, final_url):
        self.soup = BS(text, "html5lib")
        self.final_url = utils.throw_away_querystring(final_url)
        self.url_with_qs = final_url
        self.follow_links = utils.follow_links_on_page(self.soup)

    def parse(self):
        # extract anchor tags' hrefs
        a_set = utils.extract_links_from_anchortags(self.soup)
        #print("raw links=", a_set)
        a_candidates = self.to_candidate_links(a_set)
        #print("after to_candidate_links=", a_candidates)

        # extract asset: media
        img_set = utils.extract_links_from_imgtags(self.soup)
        img_candidates = self.to_candidate_links(img_set)

        vids, posters = utils.extract_links_from_video(self.soup)
        video_candidates = self.to_candidate_links(vids)
        poster_candidates = self.to_candidate_links(posters)

        img_candidates.extend(poster_candidates)

        # extract asset: JS
        script_set = utils.extract_links_from_scripttags(self.soup)
        script_candidates = self.to_candidate_links(script_set)

        # extract asset: CSS
        css_set = utils.extract_links_from_css(self.soup)
        css_candidates = self.to_candidate_links(css_set)

        # Uses ``self.url_with_qs`` to handle dynamically generated
        # pages that VARY depending on input querystring. E.g.::
        #
        #     https://www.digitalocean.com/community?page=1
        #     https://www.digitalocean.com/community?page=2
        retval = Responder.with_args(self.url_with_qs,
                                     a_candidates,
                                     frozenset(script_candidates),
                                     frozenset(css_candidates),
                                     frozenset(img_candidates),
                                     frozenset(video_candidates))
        Responder.with_args_for_graph(retval)
        return retval

    def close(self):
        if self.soup:
            self.soup.decompose()

    def __repr__(self):
        return '<Parser [%d]>' % (id(self))

    def to_candidate_links(self, set_of_hrefs):
        """ Returns only URLs which are relative to ``self.final_url``.
        NOTE: This is problematic because many websites serve static
              assets from CDNs or special static servers; so this
              function will eliminate all of them.

        :param set_of_hrefs: raw URLs absolute and relative
        :return: a :class:`list` which may contain duplicates.
        """
        tmp = []
        for href in set_of_hrefs:
            if not href:
                continue
            if self.absolute_urls1_re.match(href):
                baseurl = utils.retain_only_scheme_and_netloc(self.final_url)
                if href.startswith(baseurl):
                    tmp.append(href)

            elif self.relative_urls_re.match(href):
                match = self.relative_urls_re.match(href)
                groups = match.groups()

                if len(groups) >= 1:
                    new_url = (utils.try_create_url_with_scheme(
                               self.final_url, groups[0]))
                    baseurl = utils.retain_only_scheme_and_netloc(
                        self.final_url)
                    if new_url.startswith(baseurl):
                        tmp.append(new_url)
            else:
                if not urljoin(self.final_url, href) == href:
                    tmp.append(urljoin(self.final_url, href))

        return tmp
