# -*- coding: utf-8 -*-
"""Generates sitemap for a single domain"""

from edderkopp import metadata


__version__ = metadata.version
__author__ = metadata.authors[0]
__license__ = metadata.license
__copyright__ = metadata.copyright
