# -*- coding: utf-8 -*-
"""Project metadata

Information describing the project.
"""

# The package name, which is also the "UNIX name" for the project.
package = 'edderkopp'
project = "edderkopp"
project_no_spaces = project.replace(' ', '')
version = '0.1'
description = 'Generates sitemap for a single domain'
authors = ['GavinB']
authors_string = ', '.join(authors)
emails = ['rubycoder@gmail.com']
license = 'MIT'
copyright = '2014 ' + authors_string
url = 'https://bitbucket.org/pontresina/'
