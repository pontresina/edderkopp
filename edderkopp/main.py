#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Program entry point"""

from __future__ import print_function

import argparse
import contextlib
import sys

from edderkopp import metadata
from edderkopp import utils
from edderkopp import app


class URLAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        #import pdb; pdb.set_trace()
        #print('%r %r %r' % (namespace, values, option_string))
        errmsg = utils.verify_url(values)
        if errmsg:
            parser.error(errmsg)
        setattr(namespace, self.dest, values)
        #print('%r %r %r' % (namespace, values, option_string))


def main(argv):
    """Program entry point.

    :param argv: command-line arguments
    :type argv: :class:`list`
    """
    author_strings = []
    for name, email in zip(metadata.authors, metadata.emails):
        author_strings.append('Author: {0} <{1}>'.format(name, email))

    epilog = '''
{project} {version}

{authors}
URL: <{url}>
'''.format(
        project=metadata.project,
        version=metadata.version,
        authors='\n'.join(author_strings),
        url=metadata.url)

    arg_parser = argparse.ArgumentParser(
        prog=argv[0],
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=metadata.description,
        epilog=epilog)
    arg_parser.add_argument(
        '-V', '--version',
        action='version',
        version='{0} {1}'.format(metadata.project, metadata.version))

    arg_parser.add_argument(
        'url', type=str, action=URLAction,
        help="starting point to crawl")

    args = arg_parser.parse_args(args=argv[1:])

    exitcode = 0
    crawler = app.Crawler(args.url)
    try:
        with contextlib.closing(crawler):
            crawler.open()
            exitcode = crawler.crawl()
    except:
        exitcode = 2

    return exitcode


def entry_point():
    """Zero-argument entry point for use with setuptools/distribute."""
    raise SystemExit(main(sys.argv))


if __name__ == '__main__':
    entry_point()
