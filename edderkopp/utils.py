# -*- coding: utf-8 -*-
"""edderkopp.utils

Module provides shareable utilities
"""
import bs4
import collections
import re
import time

from functools import wraps
from urlparse import urlparse, urlunsplit, urlsplit
#from requests.structures import CaseInsensitiveDict

INVALID_SCHEMES = "Bad url - acceptable schemes are {'https', 'http'}"
MISSING_NETLOC = "Bad url - missing netloc e.g. 98.138.253.109, yahoo.com:443"
INVALID_PATH = "Bad url - path malformed. http://example.com/foo/cgi?a1"


def verify_url(u):
    """ Simple checks for URL supplied at command line

        :return: ``None`` if ok, otherwise an error message.
        """
    try:
        o = urlparse(u)
    except:
        return INVALID_PATH

    if o.scheme not in ['https', 'http']:
        return INVALID_SCHEMES
    if not o.netloc:
        return MISSING_NETLOC
    if o.path:
        if not o.path.startswith("/"):
            return INVALID_PATH
    return None


def soupguard(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if not isinstance(args[0], bs4.BeautifulSoup):
            raise ValueError('argument is not a `bs4.BeautifulSoup`')
        return f(*args, **kwargs)
    return wrapper


def setguard(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if not isinstance(args[1], collections.Set):
            raise ValueError('argument not a `collections.Set`')
        return f(*args, **kwargs)
    return wrapper


# @TODO useful only if parser is `html.parser`
#strainer_tag_meta = SoupStrainer(name="meta")
#soup = BS(html_doc, "html.parser", parse_only=strainer_tag_meta)

meta_re = re.compile(r'meta', flags=re.I)
robots_re = re.compile(r'robots', flags=re.I)
nofollow_re = re.compile(r".*?nofollow.*?", flags=re.I)
none_re = re.compile(r"none", flags=re.I)
inlinejs_re = re.compile(r'javascript:.*', flags=re.I)


@soupguard
def follow_links_on_page(html):
    """
    :param html: instance of `bs4.BeautifulSoup`
    :return: ``True`` if no page-wide meta tag to disable following links.
             ``False`` otherwise.
    :raises: ``ValueError`` if param ``html`` is invalid

    <META NAME="ROBOTS" CONTENT="NONE"> means NOFOLLOW by default.
    @see http://googlewebmastercentral.blogspot.ca/2007/03/
                                 using-robots-meta-tag.html
    """
    results = html.find_all(name=meta_re, attrs={"name": robots_re})
    for meta in results:
        #d = CaseInsensitiveDict(data=meta.attrs)
        if 'content' in meta.attrs:
            content = meta.attrs.get('content', '')
            if nofollow_re.match(content) is not None:
                return False
            if none_re.match(content) is not None:
                return False
    return True


def pred_rel(attr):
    return attr is None or nofollow_re.match(attr) is None


def pred_href(attr):
    return (attr is not None and not attr.startswith("#") and
            inlinejs_re.match(attr) is None)


@soupguard
def extract_links_from_anchortags(html):
    """
    :param html: instance of `bs4.BeautifulSoup`
    :return: a ``list`` of candidate anchor tags
    """
    filtered1 = html.find_all("a", attrs={"rel": pred_rel,
                                          "href": pred_href})
    return [a.get("href", "").lower() for a in filtered1]


@soupguard
def extract_links_from_scripttags(html):
    """
    :param html: instance of `bs4.BeautifulSoup`
    :return: a ``frozenset`` of candidate script assets
    """
    filtered = html.find_all("script", attrs={"src": lambda a: a is not None})
    return frozenset([a.get("src", "").lower() for a in filtered])


@soupguard
def extract_links_from_imgtags(html):
    """
    It ignores images inside:
    - meta
    - link
    - CSS

    :param html: instance of `bs4.BeautifulSoup`
    :return: a ``frozenset`` of candidate images
    """
    imgs = html.find_all("img", attrs={"src": lambda a: a is not None})
    return frozenset([a.get("src", "").lower() for a in imgs])


@soupguard
def extract_links_from_video(html):
    """
    :param html: instance of `bs4.BeautifulSoup`
    :return: a tuple (VID, IMG) where by:
         - VID is a ``frozenset`` of candidate hrefs to video media
         - IMG is a ``frozenset`` of candidate hrefs to video poster images
    """
    videos = []
    posters = []

    videotags = html.find_all("video")
    for vid in videotags:
        poster = vid.get("poster", "")
        if poster:
            posters.append(poster)
        avideo = vid.get("src", "")
        if avideo:
            videos.extend([avideo.lower()])
            continue
        sources = vid.find_all("source", {"src": lambda a: a is not None})
        videos.extend([s.get("src", "").lower() for s in sources])

    return (frozenset(videos), frozenset(posters))


@soupguard
def extract_links_from_css(html):
    """
    :param html: instance of `bs4.BeautifulSoup`
    :return: a ``frozenset`` of candidate CSS assets
    """
    css = html.find_all("link", attrs={"type": "text/css",
                        "href": lambda a: a is not None})
    return frozenset([a.get("href", "").lower() for a in css])


def throw_away_querystring(url):
    if verify_url(url) is not None:
        raise ValueError('url [{0}] invalid'.format(url))
    res = urlparse(url)
    return urlunsplit([res.scheme, res.netloc, res.path, '', ''])


def retain_only_scheme_and_netloc(url):
    if verify_url(url) is not None:
        raise ValueError('url [{0}] invalid'.format(url))
    parts = urlsplit(url)
    return urlunsplit([parts.scheme, parts.netloc, '', '', ''])


def try_create_url_with_scheme(base_url, relative_url):
    if verify_url(base_url) is not None:
        raise ValueError('base_url [{0}] invalid'.format(base_url))
    base = urlsplit(base_url)
    rel = urlsplit("http://" + relative_url)
    return urlunsplit([base.scheme] + list(rel[1:]))


def dotfilename(url, alt='edderkopp', t=time.time):
    """
    :return: a ``str`` to be used as filename for the dot file
    """
    basename = alt
    if verify_url(url) is None:
        parts = urlsplit(url)
        if parts.netloc:
            basename = ''.join(c for c in parts.netloc if c not in '@?:.')

    return '%s_%s.dot' % (basename, int(t()))
