# -*- coding: utf-8 -*-
from pytest import raises

import pytest
parametrize = pytest.mark.parametrize

from edderkopp import metadata
from edderkopp.main import main
from edderkopp.utils import INVALID_SCHEMES


class TestMain(object):
    @parametrize('malformedurl', ['ftp://foo.com/', 'http//foo.org'])
    def test_error_in_url(self, malformedurl, capsys):
        with raises(SystemExit) as exc_info:
            main(['progname', malformedurl])
        out, err = capsys.readouterr()
        assert INVALID_SCHEMES in err
        assert exc_info.value.code == 2

    @parametrize('helparg', ['-h', '--help'])
    def test_help(self, helparg, capsys):
        with raises(SystemExit) as exc_info:
            main(['progname', helparg])
        out, err = capsys.readouterr()
        assert 'usage' in out
        assert 'progname' in out
        assert exc_info.value.code == 0

    @parametrize('versionarg', ['-V', '--version'])
    def test_version(self, versionarg, capsys):
        with raises(SystemExit) as exc_info:
            main(['progname', versionarg])
        out, err = capsys.readouterr()
        assert err == '{0} {1}\n'.format(metadata.project, metadata.version)
        assert exc_info.value.code == 0
