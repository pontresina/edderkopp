from requests.structures import CaseInsensitiveDict


class MockResponse(object):
    """ fake ``requests.models.Response`` """
    def __init__(self, code, url=None, body=''):
        self.status_code = code
        self.url = url
        self.text = body
        self.headers = CaseInsensitiveDict()


test_urls = [
    ('http://digitalocean.com',
     'https://www.digitalocean.com/'),
    ('http://google.com',
     'http://www.google.ca/?gfe_rd=cr&ei=jVyGU8zQMazP8geF6YGoDg'),
    ('http://ask.fm',
     'http://ask.fm')]
