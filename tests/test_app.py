# -*- coding: utf-8 -*-
from mock import patch, MagicMock
from common import MockResponse, test_urls
from pytest import raises
from edderkopp.app import (URLFetcher, FetchError, Parser,
                           Responder, Asset, Robots, Crawler)
from edderkopp import utils

import re
import http_snippets
import logging
import os
import pytest
import networkx
import http_statuscodes as HTTP


@pytest.fixture
def useragent():
    return {'User-Agent': 'FakeSpider/1.0'}


@pytest.fixture
def fetcher(useragent):
    return URLFetcher(useragent)


@pytest.fixture
def regex():
    return re.compile(r".*?\[.*?\].*?\[(?P<httpcode>[45][0-9][0-9])\]")


def test_useragent(fetcher):
    GOOGLE = 'http://www.google.com'
    with patch('requests.get') as mockget:
        mockget.return_value = MockResponse(400)
        fetcher.fetch(GOOGLE)
    mockget.assert_called_with(GOOGLE,
                               headers={'User-Agent': 'FakeSpider/1.0'},
                               allow_redirects=True)


def test_asset_class():
    url1 = u'http://httpbin.org/foo.png'
    assert hash(url1) == 1171463799
    a1 = Asset(u'foo.png', 1171463799)
    assert hash(a1) == hash(url1)
    assert repr(a1) == u"Asset(u'foo.png', 1171463799)"

    # tuple unpack
    aname, ahash = a1
    assert aname == u'foo.png'
    assert ahash == 1171463799

    # equality
    a2 = Asset(u'foo.png', 1171463799)
    assert a1 == a2
    assert a2 == a1
    assert hash(a1) == hash(a2)

    # identity
    assert not a1 is a2

    # ordering
    url2 = u'https://www.digitalocean.com/asset/a.mp4'
    assert hash(url2) == 302691733
    a2 = Asset(u'a.mp4', 302691733)
    assert hash(a2) == hash(url2)
    assert a1 != a2
    assert a2 < a1
    assert a1 > a2


def test_url_connect_refused(reqs_connectrefused, fetcher, single_url):
    with raises(FetchError) as excinfo:
        fetcher.fetch(single_url)
    assert excinfo.exconly().index('Connection refused') >= 0
    assert excinfo.value.url == single_url


def test_url_connect_nameunknown(reqs_nameunknown, fetcher, single_url):
    with raises(FetchError) as excinfo:
        fetcher.fetch(single_url)
    assert excinfo.exconly().index('Name or service not known') >= 0
    assert excinfo.value.url == single_url


@pytest.fixture(params=HTTP.range400.keys())
def reqs_40x(monkeypatch, request, start_urls):
    def mockget(url, **kwargs):
        return MockResponse(request.param, start_urls[0].original)
    monkeypatch.setattr("requests.get", mockget)


def test_http_with_400responses_sans_redirect(reqs_40x, fetcher,
                                              regex, start_urls, caplog):
    caplog.setLevel(logging.INFO)
    response = fetcher.fetch(start_urls[0].original)
    match = regex.match(caplog.text())
    assert response == {'url': 'http://digitalocean.com',
                        'explore': [],
                        'reason': (902, u'HTTP {0}'.format(match.group(1))),
                        'assets': {
                            'js': frozenset([]),
                            'css': frozenset([]),
                            'img': frozenset([]),
                            'vid': frozenset([])}}


@pytest.fixture(params=HTTP.range500.keys())
def reqs_50x(monkeypatch, request, start_urls):
    finalurl = start_urls[0].original
    try:
        request.function.func_name.index('avec_redirect')
        finalurl = start_urls[0].redirected
    except ValueError:
        pass

    def mockget(url, **kwargs):
        return MockResponse(request.param, finalurl)
    monkeypatch.setattr("requests.get", mockget)


def test_http_with_500responses_avec_redirect(reqs_50x, fetcher, regex,
                                              start_urls, caplog):
    expected_url = 'https://www.digitalocean.com/'
    expected_log = 'ignored [%s] due to HTTP response [50' % (expected_url)
    caplog.setLevel(logging.INFO)
    response = fetcher.fetch(start_urls[0].original)
    assert expected_log in caplog.text()
    match = regex.match(caplog.text())
    assert response == {'url': expected_url,
                        'reason': (902, u'HTTP {0}'.format(match.group(1))),
                        'explore': [],
                        'assets': {
                            'js': frozenset([]),
                            'css': frozenset([]),
                            'img': frozenset([]),
                            'vid': frozenset([])}}


# @see used in testcase ``test_urlfetch_to_candidate_links``.
map2expected = {
    '1_https://www.digitalocean.com/': set(
        [u'https://www.digitalocean.com/company/careers/',
         u'https://www.digitalocean.com/',
         u'https://www.digitalocean.com/features/']),
    '1_http://www.google.ca/': set(
        [u'http://www.google.ca/company/careers/',
         u'http://www.google.ca/',
         u'http://www.google.ca/features/']),
    '1_http://ask.fm': set([u'http://ask.fm/company/careers/',
                            u'http://ask.fm/',
                            u'http://ask.fm/features/']),
    '2_https://www.digitalocean.com/': set(
        [u'https://www.digitalocean.com/matt.html',
         u'https://www.digitalocean.com/foo.html']),
    '2_http://www.google.ca/': set(
        [u'http://www.google.ca/matt.html',
         u'http://www.google.ca/foo.html']),
    '2_http://ask.fm': set([u'http://ask.fm/matt.html',
                            u'http://ask.fm/foo.html']),
    '3_https://www.digitalocean.com/': set(
        [u'https://www.digitalocean.com/index.jsp']),
    '3_http://www.google.ca/': set([u'http://www.google.ca/policy/']),
    '3_http://ask.fm': set([u'http://ask.fm/index.html']),
}


@pytest.fixture(params=test_urls, scope="module")
def parser(request):
    _, redirected = request.param
    parser = Parser("", redirected)
    request.addfinalizer(parser.close)
    return parser


@pytest.fixture(params=[
    http_snippets.set_digocean_hrefs,
    http_snippets.set_html_small_hrefs,
    http_snippets.set_relative_hrefs,
    ])
def href_candidates(request, parser):
    # @see ``map2expected`` above.
    # We add one because ``request.param_index`` is zero-based.
    return tuple([request.param, str(request.param_index + 1), parser])


def test_urlfetch_to_candidate_links(href_candidates):
    arg, id_of_hrefs, parser = href_candidates
    key = '{0}_{1}'.format(id_of_hrefs, parser.final_url)
    assert set(parser.to_candidate_links(arg)) == map2expected[key]


@pytest.mark.parametrize("hrefs, expected", [
    (['/html2', 'assets/js/foo.js', 'mailto:ruby@google.com'],
     ['http://httpbin.org/html2', 'http://httpbin.org/assets/js/foo.js']),
    (['http://httpbin.org/status/500'],
     ['http://httpbin.org/status/500']),
    (['//httpbin.org/deny'], ['http://httpbin.org/deny']),
])
def test_urlfetch_to_candidate_links_part2(hrefs, expected):
    parser = Parser("", "http://httpbin.org/html1")
    assert parser.to_candidate_links(hrefs) == expected


# Mock result 1 of parsing via ``edderkopp.app.Parser``
# @see fixture ``parsed_dict`` below.
google = {'url': u'http://www.google.ca/',
          'explore': [u'http://www.google.ca/options/'],
          'assets': {'vid': frozenset([]), 'img': frozenset([]),
                     'css': frozenset([]), 'js': frozenset([])}}

# Mock result 2 of parsing via ``edderkopp.app.Parser``
# @see fixture ``parsed_dict`` below.
digoce = {
    'url': u'https://www.digitalocean.com/',
    'explore': [u'https://www.digitalocean.com/apps/'],
    'assets': {
        'vid': frozenset([u'https://www.digitalocean.com/asset/a.mp4',
                          u'https://www.digitalocean.com/asset/c.webm']),
        'img': frozenset([u'https://www.digitalocean.com/assets/i.jpg']),
        'css': frozenset([u'https://www.digitalocean.com/assets/4b7.css']),
        'js': frozenset([u'https://www.digitalocean.com/assets/33.js'])}}


@pytest.fixture(params=[
    (google, google),
    (digoce,
        {'url': u'https://www.digitalocean.com/',
         'explore': [u'https://www.digitalocean.com/apps/'],
         'assets': {'css': frozenset([Asset('4b7.css', 1472044125)]),
                    'img': frozenset([Asset('i.jpg', 520012351)]),
                    'js': frozenset([Asset('33.js', -214393480)]),
                    'vid': frozenset([Asset(u'a.mp4', 302691733),
                                      Asset(u'c.webm', 804909212)])}}),
    ({}, {}),
    ])
def parsed_dict(request):
    return request.param


def test_responder_with_args_for_graph(parsed_dict):
    arg, expected = parsed_dict
    Responder.with_args_for_graph(arg)  # side-effects mutates ``arg``
    assert arg == expected


def test_returning_application_json(json_response, start_urls, fetcher):
    response = fetcher.fetch(start_urls[0].original)
    assert 'reason' in response
    assert response['reason'] == (901, u'application/json')


def test_robotcache(monkeypatch, useragent, single_url):
    GOOGLE, YAHOO = 'http://www.google.com', 'http://www.yahoo.com'
    ua = useragent['User-Agent']

    mock = MagicMock(name="RobotsCache")
    robots_cache = mock.return_value
    robots_cache.allowed.side_effect = [False, True]
    robots_cache.delay.return_value = 3.5

    # LESSON: will not work because ``from reppy.cache import RobotsCache``
    #monkeypatch.setattr("reppy.cache.RobotsCache", MockRobots)
    monkeypatch.setattr("edderkopp.app.RobotsCache", mock)
    r = Robots(useragent, single_url)

    # Test ``delay`` method
    assert r.delay == 2.0
    r.download_robots_txt()
    assert r.delay == 3.5
    robots_cache.delay.assert_called_with(single_url, ua)
    assert robots_cache.delay.call_count == 1

    # Test ``allowed`` method
    assert not r.allowed(GOOGLE)
    robots_cache.allowed.assert_called_with(GOOGLE, ua)

    assert r.allowed(YAHOO)
    robots_cache.allowed.assert_called_with(YAHOO, ua)

    assert robots_cache.allowed.call_count == 2


def test_fetch_httpbin(httpbin, monkeypatch, fetcher):
    txt, url, expected = httpbin

    def mockget(u, **kwargs):
        resp = MockResponse(200, url, txt)
        resp.headers.update({'Content-Type': 'text/html'})
        return resp

    monkeypatch.setattr("requests.get", mockget)
    assert fetcher.fetch(url) == expected

HTTPBIN500 = 'http://httpbin.org/status/500'
HTTPBIN418 = 'http://httpbin.org/status/418'
HTTPBIN204 = 'http://httpbin.org/status/204'
HTTPBIN401 = 'http://httpbin.org/basic-auth/user/passw'
HTTPBIN_H1 = 'http://httpbin.org/html1'
HTTPBIN_H2 = 'http://httpbin.org/html2'

map4httpbin = {
    'http://httpbin.org': lambda: http_snippets.httpbin_root,
    'http://httpbin.org/': lambda: http_snippets.httpbin_root,
    HTTPBIN500: lambda: http_snippets.httpbin_40x_50x(HTTPBIN500, 500),
    HTTPBIN418: lambda: http_snippets.httpbin_40x_50x(HTTPBIN418, 418),
    HTTPBIN401: lambda: http_snippets.httpbin_40x_50x(HTTPBIN401, 401),
    HTTPBIN204: lambda: http_snippets.httpbin_204,
    HTTPBIN_H1: lambda: http_snippets.httpbin_html(1),
    HTTPBIN_H2: lambda: http_snippets.httpbin_html(2)
}


@pytest.fixture
def httpbin_dot_org(monkeypatch):
    """
    Mock website `http://httpbin.org/` to test ``Crawler``
    A digraph of        +------+
    related pages       |      |
                        v      |
                 httpbin.org---+
                  |     |  ^
                  |     |  |
                  v     v  |
               html1<---html2
    """
    def mock_fetch(self, url):
        if url in map4httpbin:
            return map4httpbin.get(url)()

        if url == u'http://httpbin.org/get':
            return http_snippets.httpbin_json

        if url == u'http://httpbin.org/die':
            raise FetchError('{0}'.format('Connrefused)'), url)

        raise RuntimeError('bad')
    monkeypatch.setattr("edderkopp.app.URLFetcher.fetch", mock_fetch)

    class MockRobotsCache(object):
        """ Implements the robots.txt::

                User-agent: *
                Crawl-delay: 3.0
                Disallow: /deny
        """
        def __init__(self, **kwargs):
            pass

        def cache(self, url):
            pass

        def delay(self, url, ua):
            return 3.0

        def allowed(self, url, ua):
            if not url:
                return False

            if 'http://httpbin.org/deny' == url.strip().lower():
                return False
            return True

        def clear(self):
            pass

    monkeypatch.setattr("edderkopp.app.RobotsCache", MockRobotsCache)


@pytest.fixture
def crawler(request):
    crawler = Crawler(u'http://httpbin.org', sleep=lambda secs: None)
    request.addfinalizer(crawler.close)
    return crawler


def test_verify_fetches(httpbin_dot_org, crawler, fetcher):
    URLs = ['http://httpbin.org/', 'http://httpbin.org',
            'http://httpbin.org/get']
    assert fetcher.fetch(URLs[0]) == fetcher.fetch(URLs[1])
    assert fetcher.fetch(URLs[2]) == http_snippets.httpbin_json


@pytest.fixture(scope="function")
def mocktmpdir(tmpdir, monkeypatch):
    def mockgettempdir():
        return tmpdir.dirname
    monkeypatch.setattr("edderkopp.app.gettempdir", mockgettempdir)


@pytest.fixture()
def mockdotfilename(monkeypatch):
    monkeypatch.setattr('edderkopp.utils.dotfilename', lambda u: 'fake.dot')


@pytest.fixture()
def graphdetails():
    retval = {}
    retval.update({'nodes': [u'http://httpbin.org/',
                             u'http://httpbin.org',
                             u'http://httpbin.org/html1',
                             u'http://httpbin.org/html2',
                             u'http://httpbin.org/deny',
                             u'http://httpbin.org/get',
                             u'http://httpbin.org/status/204',
                             u'http://httpbin.org/status/418',
                             u'http://httpbin.org/status/500',
                             u'http://httpbin.org/die',
                             u'http://httpbin.org/basic-auth/user/passw',
                             Asset(u'template.js', 1484605685),
                             Asset(u'main.css', 1315410294),
                             Asset(u'github.jpg', -112548750),
                             Asset(u'html.jpg', -603441762)]})
    retval.update({'edges': [
        (u'http://httpbin.org/', u'http://httpbin.org'),
        (u'http://httpbin.org/', u'http://httpbin.org/status/204'),
        (u'http://httpbin.org/', u'http://httpbin.org/'),
        (u'http://httpbin.org/', u'http://httpbin.org/html1'),
        (u'http://httpbin.org/', u'http://httpbin.org/html2'),
        (u'http://httpbin.org/', u'http://httpbin.org/deny'),
        (u'http://httpbin.org/', u'http://httpbin.org/get'),
        (u'http://httpbin.org/', Asset(u'github.jpg', -112548750)),
        (u'http://httpbin.org/', Asset(u'template.js', 1484605685)),
        (u'http://httpbin.org/', Asset(u'main.css', 1315410294)),
        (u'http://httpbin.org/html1', u'http://httpbin.org/status/500'),
        (u'http://httpbin.org/html1', Asset(u'html.jpg', -603441762)),
        (u'http://httpbin.org/html2', u'http://httpbin.org/html1'),
        (u'http://httpbin.org/html2', u'http://httpbin.org'),
        (u'http://httpbin.org/html2',
         u'http://httpbin.org/basic-auth/user/passw'),
        (u'http://httpbin.org/html2', u'http://httpbin.org/status/418'),
        (u'http://httpbin.org/html2', u'http://httpbin.org/die'),
        (u'http://httpbin.org/html2', Asset(u'html.jpg', -603441762))]})
    return retval


def test_crawl_httpbin(mocktmpdir, mockdotfilename, tmpdir,
                       httpbin_dot_org, crawler):
    """ SUT (System under test) is the event consumer """
    mock = MagicMock(name="EventSubscriber")
    crawler.subscriber = mock
    crawler.open()
    crawler.crawl()

    # Assertions about the event stream
    mock.denied.assert_called_once_with(u'http://httpbin.org/deny')
    assert mock.enqueued.call_count == 10
    assert mock.problem.call_count == 5
    assert mock.fetch_error.call_count == 1

    expected_call_order = """[call.enqueued(u'http://httpbin.org', True),
 call.enqueued(u'http://httpbin.org/status/204'),
 call.enqueued(u'http://httpbin.org/html1'),
 call.enqueued(u'http://httpbin.org/html2'),
 call.enqueued(u'http://httpbin.org/deny'),
 call.enqueued(u'http://httpbin.org/get'),
 call.problem((903, u'No Content'), u'http://httpbin.org/status/204'),
 call.enqueued(u'http://httpbin.org/status/500'),
 call.enqueued(u'http://httpbin.org/basic-auth/user/passw'),
 call.enqueued(u'http://httpbin.org/status/418'),
 call.enqueued(u'http://httpbin.org/die'),
 call.denied(u'http://httpbin.org/deny'),
 call.problem((901, u'application/json'), u'http://httpbin.org/get'),
 call.problem((902, u'HTTP 500'), u'http://httpbin.org/status/500'),
 call.problem((902, u'HTTP 401'), u'http://httpbin.org/basic-auth/user/passw'),
 call.problem((902, u'HTTP 418'), u'http://httpbin.org/status/418'),
 call.fetch_error(FetchError('Connrefused)',), u'http://httpbin.org/die')]"""
    assert repr(mock.mock_calls) == expected_call_order

    # Assertions about generated Graphiz ``dot`` file
    tmp_file = tmpdir.dirname + os.sep + utils.dotfilename(crawler.url)
    with open(tmp_file) as f:
        contents = f.readlines()
        assert len(contents) == 21
        assert contents[0].index('digraph sitemap') >= 0


def test_crawl_httpbin2(monkeypatch, mocktmpdir, mockdotfilename,
                        httpbin_dot_org, crawler, graphdetails):
    """ SUT is the Graph API """
    #mock_write_dot = MagicMock(name='networkx')
    #monkeypatch.setattr('networkx.write_dot', mock_write_dot)
    mock = MagicMock(name="EventSubscriber")
    crawler.subscriber = mock

    crawler.open()
    crawler.crawl()

    # Assertions about the directed graph that was created
    thegraph = crawler.digraph
    assert type(thegraph) == networkx.classes.digraph.DiGraph
    assert thegraph.number_of_nodes() == len(graphdetails['nodes'])
    assert thegraph.number_of_edges() == len(graphdetails['edges'])

    for node in graphdetails['nodes']:
        assert node in thegraph.nodes()

    for edge in graphdetails['edges']:
        assert edge in thegraph.edges()
