# -*- coding: utf-8 -*-

import pytest
from pytest import raises
from pytest import mark
from edderkopp import utils, app


@pytest.mark.parametrize("url, expected", [
    mark.xfail(('', '')),
    mark.xfail(('ssh://hg@bitbucket.com/a/b', 'ssh://hg@bitbucket.com')),
    ('https://u@google.com?a=1', 'https://u@google.com'),
    ('http://yahoo.com:8080/a/b?b=1', 'http://yahoo.com:8080'),
])
def test_retain_only_scheme_and_netloc(url, expected):
    assert utils.retain_only_scheme_and_netloc(url) == expected


@pytest.mark.parametrize("url, expected", [
    ('https://google.com/policy?a=1', 'https://google.com/policy'),
    ('https://google.com/policy/?a=1', 'https://google.com/policy/'),
    ('http://yahoo.com:8080/a/b?b=1', 'http://yahoo.com:8080/a/b'),
])
def test_throw_away_querystring(url, expected):
    assert utils.throw_away_querystring(url) == expected


@pytest.mark.parametrize("url, alt, expected", [
    ('https://google.com/policy?a=1', None, 'googlecom_1402522353.dot'),
    ('https://user@google.com:443/policy/', None,
     'usergooglecom443_1402522353.dot'),
    ('mailto:foobar@gmail.com', 'foo', 'foo_1402522353.dot'),
    ('mailto:foobar@gmail.com', None, 'edderkopp_1402522353.dot'),
    (None, None, 'edderkopp_1402522353.dot'),
])
def test_dotfilename(url, alt, expected):
    kwargs = {'t': lambda: 1402522353}
    if alt:
        kwargs.update({'alt': alt})
    assert utils.dotfilename(url, **kwargs) == expected


@pytest.mark.parametrize("base, rel, expected", [
    ('http://httpbin.org/html1', '//httpbin.org/img/funny.gif',
        'http://httpbin.org/img/funny.gif'),
    ('https://httpbin.org/html1', '//httpbin.org/img/funny.gif',
        'https://httpbin.org/img/funny.gif'),
])
def test_try_create_url_with_scheme(base, rel, expected):
    assert utils.try_create_url_with_scheme(base, rel) == expected


@pytest.fixture(params=["random string", 123, None])
def not_beautifulsoup(request):
    return request.param


def test_extract_anchortags(bodies2):
    bs, expected = bodies2
    assert utils.extract_links_from_anchortags(bs) == expected


def test_extract_anchortags_with_bad_arg(not_beautifulsoup):
    with raises(ValueError) as excinfo:
        utils.extract_links_from_anchortags(not_beautifulsoup)
    assert excinfo.exconly().index('argument is not a `bs4') >= 0


def test_extract_scripttag(scripts):
    bs, expected = scripts
    assert utils.extract_links_from_scripttags(bs) == expected


def test_extract_imgtag(images):
    bs, expected = images
    assert utils.extract_links_from_imgtags(bs) == expected


def test_extract_css(css):
    bs, expected = css
    assert utils.extract_links_from_css(bs) == expected


def test_extract_videotag(videos):
    bs, expvids, expposters = videos
    actualvids, actualposters = utils.extract_links_from_video(bs)
    assert actualvids == expvids
    assert actualposters == expposters


def test_follow_links_on_page(bodies1):
    bs, expected = bodies1
    assert utils.follow_links_on_page(bs) == expected


def test_follow_links_on_page_with_bad_arg(not_beautifulsoup):
    with raises(ValueError) as excinfo:
        utils.follow_links_on_page(not_beautifulsoup)
    assert excinfo.exconly().index('argument is not a `bs4') >= 0


def test_ishtml(responses1):
    response, expected_html, _ = responses1
    ok, msg = app.URLFetcher.is_html(response)
    assert ok == expected_html
    if not ok:
        assert msg == '{0}'.format(response.headers['content-type'])


def test_notempty_body(responses1):
    response, _, expected_emptybody = responses1
    ok, msg = app.URLFetcher.not_empty(response)
    assert ok == expected_emptybody
    if not ok:
        assert msg == 'empty body'


def test_urlcheck(udata):
    (url, expected) = udata
    actual = utils.verify_url(url)
    if expected is None:
        assert actual is None
    else:
        assert actual == expected
