# -*- coding: utf-8 -*-
html_small = """
<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
  <title>Small page</title>
  <Meta name="description" content="Small">
  <META name="keywords" content="small">
  <meta name="robots" content="index, follow" />
  <meta property="og:image" content="http://ask.fm/images/155x155.png" />
  <link rel="image_src" href="http://ask.fm/images/155x155.png" />
  <link rel="icon" href="favicon.ico">
  <link rel="apple-touch-icon" sizes="72x72" href="/icon-72x72.png"/>
  <link href="//ask.fm/assets/css/screen.css" media="screen"
      rel="stylesheet" type="text/css"/>
  <link href="/assets/css/style-54b74a34.css" media="screen"
      rel="stylesheet" type="text/css"/>
  <link href="http://cdn3.vox-cdn.com/stylesheets/7194.css"
      media="all" rel="stylesheet" type="text&#x2F;css" />
  <script>
     document.domain = "ask.fm";
  </script>
  <script src="/assets/pkg/base-0.js"></script>
  <script src="assets/pkg/base-1.js"></script>
  <script src="//use.typekit.net/wix0mlm.js" type="text/javascript"></script>
  </script>
</head>
<body>
  Hello Lilliputians
  <a class="gothere" href="#">click1</a>
  <a class="gothere2" href="#name">click19</a>
<div>
  <a href="foo.html">click2</a>
  <a href="bar.html" rel="nofollow">click5</a>
  <A HREF="bar.html" REL="NOFOLLOW">click6</a>
  <A HREF="matt.html" REL="friend">click7</a>
  <A href="foo.html">click2a</a>
  <a href="/foo.html">click3</a>
  <a href="/FOO.html">click3a</a>
  <A HREF="http://www.google.com">click4</a>
</div>
<script type="text/javascript"
  src="http://www.googleadservices.com/pagead/conversion.js"></script>
  <img alt="" class="head" id="face_64842562"
      src="/assets/354/thumb_tiny/file.jpg" />
  <video class="feature-video" height="100%"
         poster="/assets/images/cover_create-e6646e64.jpg"
         preload="none">
    <source src="/assets/video/create-a27a9cbd.mp4" type="video/mp4">
    </source>
    <source src="/assets/video/create-e68d7b3c.webm" type="video/webm">
    </source>
    Your browser does not support the video tag.
  </video>
  <img src="//www.digitalocean.com/assets/john_resig.jpg"/>
  <img src="http://i.cbc.ca/16x9_300/thailand-protest.jpg" />
  <ul>
  <li class="one-click"
      data-video-url="/assets/video/one-click-970ab2c1.webm"
      data-url-bar="https://cloud.digitalocean.com/droplets">
    <a href="#">
      <div class="icon"><span></span></div>
      <h3>1-Click Installs</h3>
      <p>Save a ton of time installing Rails,<br>Docker,
      GitLab and more with 1-click.</p>
    </a>
  </li>
  </ul>
</body>
</html>
"""

set_digocean_hrefs = set([u'/company/careers/', u'/features/', u'/', u'',
                          u'https://cloud.digitalocean.com/login'])
#set_html_small_hrefs = set([u'foo.html', u'matt.html',
#                            u'/foo.html', u'http://www.google.com'])

set_html_small_hrefs = ['foo.html', 'matt.html', 'foo.html', '/foo.html',
                        '/foo.html', 'http://www.google.com']

set_relative_hrefs = set([u'//www.digitalocean.com/index.jsp',
                          u'//ask.fm/index.html',
                          u'//www.google.ca/policy/'])

text_plain = """
      \|||/
      (o o)
--ooO--(_)--Ooo--
"""

html_follow1 = """
<!doctype html>
<html data-placeholder-focus="false">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="description" content="abc">
    <meta name="viewport" content="width=1200, maximum-scale=1" />
  </head>
  <body>blah blah
  <a href="javascript:void(0)" class="link-login">Login</a>
  <a href="https://itunes.apple.com/" target="_blank">
     <img src="/images/buttons/appStoreBadge_welcome.png" />
     <a href="//{0}/login/">Login</a>
  </a>
  <video src="video.webm" controls>
  </video>
  </body>
</html>
"""

html_follow2 = """
<!doctype html>
<html data-placeholder-focus="false">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <META NAME="ROBOTS" CONTENT="FOLLOW, INDEX">
    <meta name="description" content="abc">
    <meta name="viewport" content="width=1200, maximum-scale=1" />
  </head>
  <body>blah blah</body>
</html>
"""

html_follow3 = """
<!doctype html>
<html data-placeholder-focus="false">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <META NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW">
    <meta name="description" content="abc">
    <meta name="viewport" content="width=1200, maximum-scale=1" />
  </head>
  <body>blah blah</body>
</html>
"""

html_nofollow1 = """
<!doctype html>
<html data-placeholder-focus="false">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <META NAME="ROBOTS" CONTENT="NOFOLLOW">
    <meta name="description" content="abc">
    <meta name="viewport" content="width=1200, maximum-scale=1" />
  </head>
  <body>blah blah</body>
</html>
"""

html_nofollow2 = """
<!doctype html>
<html data-placeholder-focus="false">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <META NAME="ROBOTS" CONTENT="INDEX,NOFOLLOW">
    <meta name="description" content="abc">
    <meta name="viewport" content="width=1200, maximum-scale=1" />
  </head>
  <body>blah blah</body>
</html>
"""

html_nofollow3 = """
<!doctype html>
<html data-placeholder-focus="false">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <META NAME="ROBOTS" CONTENT="NOFOLLOW,NOINDEX">
    <meta name="description" content="abc">
    <meta name="viewport" content="width=1200, maximum-scale=1" />
  </head>
  <body>blah blah</body>
</html>
"""

html_nofollow4 = """
<!doctype html>
<html data-placeholder-focus="false">
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <META NAME="ROBOTS" CONTENT="NONE">
    <meta name="description" content="abc">
    <meta name="viewport" content="width=1200, maximum-scale=1" />
  </head>
  <body>blah blah</body>
</html>
"""

html_httpbin_root = """
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="description" content="HTTP Client Testing Service">
    <meta name="viewport" content="width=1200, maximum-scale=1" />
    <title>httpbin(1): HTTP Client Testing Service</title>
    <link href="//httpbin.org/assets/css/main.css" media="screen"
        rel="stylesheet" type="text/css"/>
    <script src="/assets/js/template.js"></script>
  </head>
  <body>
  <img src="assets/img/github.jpg" alt="fork me on github" />
  <div>
      <h1>httpbin(1): HTTP Request & Response Service</h1>
  </div>
  <div>
      Freely hosted in <a href="http://httpbin.org">HTTP</a>, HTTPS &
      <a href="http://eu.httpbin.org/">EU</a> flavors.
      <a href="http://httpbin.org/status/204">204</a>
  </div>
  <div>
  <ul><strong>ENDPOINTS</strong>
  <li>
    <a href="/">/</a> This page.
  </li>
  <li>
    <a href="/html1">/html1</a> Renders a HTML Page #1.
  </li>
  <li>
    <a href="/html2">/html2</a> Renders a HTML Page #2.
  </li>
  <li>
    <a href="//httpbin.org/deny">/deny</a> Denied by robots.txt file.
  </li>
  <li>
  <a href="/get">/get</a> Returns GET data.
  </li>
  </ul>
  </div>
  <div>
      <h3>AUTHOR</h3>
  </div>
  <div>
  A <a href="http://github.com/kennethreitz/httpbin">Kenneth Reitz</a> project
  </div>
  </body>
</html>
"""

html_httpbin_html1 = """
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="HTTP Client Testing Service">
    <meta name="viewport" content="width=1200, maximum-scale=1" />
    <title>httpbin(1): HTML1</title>
  </head>
  <body>
  <img src="assets/img/html.jpg" alt="HTML" />
  <div>
      <h1>httpbin(1): HTML 1</h1>
  </div>
  <div>
      <a href="http://httpbin.org/status/500">/status/500</a>
  </div>
  <span>Luiz Fernando Bindi (<a href="mailto:luizfcb at uol.com.br">
        luizfcb at uol.com.br</a>)</span>
  </body>
</html>
"""

html_httpbin_html2 = """
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="description" content="HTTP Client Testing Service">
    <meta name="viewport" content="width=1200, maximum-scale=1" />
    <title>httpbin(1): HTML2</title>
  </head>
  <body>
  <img src="/assets/img/html2.jpg" alt="HTML" />
  <div>
    <a href="http://httpbin.org">Home</a>
  </div>
  <div>
    <ol>
    <li>
      <a href="//httpbin.org/basic-auth/user/passw">Connect</a>
    </li>
    <li>
      <a href="http://httpbin.org/status/418">418</a>
    </li>
    <li>
      <a href="/html1">/html1</a>
    </li>
    <li>
      <a href="http://httpbin.org/die">Simulate FetchError</a>
    </li>
    </ol>
  </div>
  </body>
</html>
"""

from edderkopp.app import Asset

httpbin_root = {'url': u'http://httpbin.org/',
                'explore': [u'http://httpbin.org',
                            u'http://httpbin.org/status/204',
                            u'http://httpbin.org/',
                            u'http://httpbin.org/html1',
                            u'http://httpbin.org/html2',
                            u'http://httpbin.org/deny',
                            u'http://httpbin.org/get'],
                'assets': {
                    'css': frozenset([Asset(u'main.css', 1315410294)]),
                    'img': frozenset([Asset(u'github.jpg', -112548750)]),
                    'js': frozenset([Asset(u'template.js', 1484605685)]),
                    'vid': frozenset([])}}

httpbin_json = {'url': u'http://httpbin.org/get',
                'explore': [],
                'reason': (901, u'application/json'),
                'assets': {'js': frozenset([]),
                           'css': frozenset([]),
                           'img': frozenset([]),
                           'vid': frozenset([])}}

httpbin_html1 = {'url': 'http://httpbin.org/html1',
                 'explore': [u'http://httpbin.org/status/500'],
                 'assets': {'vid': frozenset([]),
                            'img': frozenset([Asset(u'html.jpg', -603441762)]),
                            'css': frozenset([]), 'js': frozenset([])}}

httpbin_html2 = {'url': 'http://httpbin.org/html2',
                 'explore':
                 [u'http://httpbin.org',
                  u'http://httpbin.org/basic-auth/user/passw',
                  u'http://httpbin.org/status/418',
                  u'http://httpbin.org/html1',
                  u'http://httpbin.org/die'],
                 'assets': {'vid': frozenset([]),
                            'img': frozenset(
                                [Asset(u'html2.jpg', 1153937127)]),
                            'css': frozenset([]), 'js': frozenset([])}}

httpbin_204 = {'url': 'http://httpbin.org/status/204',
               'explore': [],
               'reason': (903, u'No Content'),
               'assets': {'js': frozenset([]), 'css': frozenset([]),
                          'img': frozenset([]), 'vid': frozenset([])}}


def httpbin_40x_50x(url, code):
    return {'url': '{0}'.format(url),
            'explore': [],
            'reason': (902, u'HTTP {0}'.format(code)),
            'assets': {'js': frozenset([]), 'css': frozenset([]),
                       'img': frozenset([]), 'vid': frozenset([])}}


def httpbin_html(ident):
    html = {'url': 'http://httpbin.org/html{0}'.format(ident),
            'explore': [],
            'assets': {'js': frozenset([]), 'css': frozenset([]),
                       'img': frozenset([Asset('html.jpg', -603441762)]),
                       'vid': frozenset([])}}
    if not ident:
        return html

    if ident == 1:
        html.update({'explore':
                     [u'http://httpbin.org/status/500']})

    elif ident == 2:
        html.update({'explore':
                     [u'http://httpbin.org',
                      u'http://httpbin.org/basic-auth/user/passw',
                      u'http://httpbin.org/status/418',
                      u'http://httpbin.org/html1',
                      u'http://httpbin.org/die']})

    return html
