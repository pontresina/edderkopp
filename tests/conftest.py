# -*- coding: utf-8 -*-
import http_snippets
import logging
import pytest
import requests

from common import MockResponse, test_urls
from edderkopp import utils

log = logging.getLogger(__name__)


@pytest.fixture(scope="session")
def single_url():
    return 'http://localhost:9413'


@pytest.fixture
def json_response(monkeypatch):
    def mockget(url, **kwargs):
        r = MockResponse(200, url, u'{"origin": "24.127.96.129"}')
        r.headers['Content-Type'] = u'application/json'
        return r
    monkeypatch.setattr("requests.get", mockget)


@pytest.fixture(params=[(), ("root/pypi",)])
def bases(request):
    log.info("<" + ''.join(request.param) + "> " + str(request.param_index))
    print dir(request)
    return request.param


@pytest.fixture(params=[
    ('', utils.INVALID_SCHEMES),
    ('garbage', utils.INVALID_SCHEMES),
    (None, utils.INVALID_PATH),
    ('http:////', utils.MISSING_NETLOC),
    ('ssh://hg@bitbucket.org/accountname/reponame/', utils.INVALID_SCHEMES),
    ('http://a[asas/]', utils.INVALID_PATH),
    ('http://digitalocean.com:8080', None),
    ('http://digitalocean.com:8080/', None),
    ('http://digitalocean.com:8080/developer', None),
    ('https://user@digitalocean.com', None,),
    ('https://user@digitalocean.com/', None,),
    ])
def udata(request):
    return request.param


msg_nameunknown = """
HTTPConnectionPool(host='localhost', port=9413):
 Max retries exceeded with url: / (Caused by <class 'socket.
gaierror'>: [Errno -2] Name or service not known)"""


@pytest.fixture(scope="function")
def reqs_nameunknown(monkeypatch):
    def mockget(url, **kwargs):
        e = requests.exceptions.ConnectionError(
            msg_nameunknown.replace('\n', ''))
        raise e
    monkeypatch.setattr("requests.get", mockget)


msg_connectrefused = """
HTTPConnectionPool(host='localhost', port=9413): Max retries
 exceeded with url: / (Caused by <class 'socket.error'>: [Err
no 111] Connection refused)"""


@pytest.fixture(scope="function")
def reqs_connectrefused(monkeypatch):
    """ ConnectionError(sockerr, request=request) """
    def mockget(url, **kwargs):
        e = requests.exceptions.ConnectionError(
            msg_connectrefused.replace('\n', ''))
        raise e
    monkeypatch.setattr("requests.get", mockget)


@pytest.fixture(scope="module")
def start_urls():
    from collections import namedtuple
    URL = namedtuple('URL', 'original redirected')
    return [URL(original=x[0], redirected=x[1]) for x in test_urls]


responses1 = [
    ('text/html', '', True, False),
    ('text/html; charset=ISO-8859-1', '', True, False),
    ('text/html; charset=ISO-8859-1', http_snippets.html_small, True, True),
    ('text/plain', '', False, False),
    ('text/plain', http_snippets.text_plain, False, True)
]


@pytest.fixture(scope="session", params=responses1)
def responses1(request):
    """ Responses with empty and non-empty contents """
    mime, text, expected_ishtml, expected_bodyempty = request.param
    r = MockResponse(200, "http://example.org", text)
    r.headers['Content-Type'] = mime
    return (r, expected_ishtml, expected_bodyempty)


@pytest.fixture(scope="session", params=[
    (http_snippets.html_follow1, True),
    (http_snippets.html_follow2, True),
    (http_snippets.html_follow3, True),
    (http_snippets.html_nofollow1, False),
    (http_snippets.html_nofollow2, False),
    (http_snippets.html_nofollow3, False),
    (http_snippets.html_nofollow4, False),
    ])
def bodies1(request):
    from bs4 import BeautifulSoup as BS
    return (BS(request.param[0], "html5lib"), request.param[1])


@pytest.fixture(scope="session", params=[
    (http_snippets.html_follow1, [u'https://itunes.apple.com/',
                                  u'//{0}/login/']),
    (http_snippets.html_follow2, []),
    (http_snippets.html_small, http_snippets.set_html_small_hrefs),
    ])
def bodies2(request):
    from bs4 import BeautifulSoup as BS
    return (BS(request.param[0], "html5lib"), request.param[1])


@pytest.fixture(scope="session", params=[
    (http_snippets.html_small, frozenset([
        u'/assets/pkg/base-0.js',
        u'assets/pkg/base-1.js',
        u'//use.typekit.net/wix0mlm.js',
        u'http://www.googleadservices.com/pagead/conversion.js']))])
def scripts(request):
    from bs4 import BeautifulSoup as BS
    return (BS(request.param[0], "html5lib"), request.param[1])


@pytest.fixture(scope="session", params=[
    (http_snippets.html_small, frozenset([
        u'/assets/354/thumb_tiny/file.jpg',
        u'//www.digitalocean.com/assets/john_resig.jpg',
        u'http://i.cbc.ca/16x9_300/thailand-protest.jpg']))])
def images(request):
    from bs4 import BeautifulSoup as BS
    return (BS(request.param[0], "html5lib"), request.param[1])


@pytest.fixture(scope="session", params=[
    (http_snippets.html_small,
     frozenset([u'/assets/video/create-e68d7b3c.webm',
                u'/assets/video/create-a27a9cbd.mp4']),
     frozenset([u'/assets/images/cover_create-e6646e64.jpg'])),
    (http_snippets.html_follow1,
     frozenset([u'video.webm']), frozenset([]))])
def videos(request):
    from bs4 import BeautifulSoup as BS
    return (BS(request.param[0], "html5lib"),
            request.param[1], request.param[2])


@pytest.fixture(scope="session", params=[
    (http_snippets.html_small, frozenset([
        u'//ask.fm/assets/css/screen.css',
        u'/assets/css/style-54b74a34.css',
        u'http://cdn3.vox-cdn.com/stylesheets/7194.css']))])
def css(request):
    from bs4 import BeautifulSoup as BS
    return (BS(request.param[0], "html5lib"), request.param[1])


@pytest.fixture(scope="session", params=[
    (http_snippets.html_httpbin_root,
     "http://httpbin.org/", http_snippets.httpbin_root),
    (http_snippets.html_httpbin_html1,
     "http://httpbin.org/html1", http_snippets.httpbin_html1),
    (http_snippets.html_httpbin_html2,
     "http://httpbin.org/html2", http_snippets.httpbin_html2),
    ])
def httpbin(request):
    return (request.param[0], request.param[1], request.param[2])
