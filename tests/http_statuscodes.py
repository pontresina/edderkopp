range100 = dict(zip([100, 101],
                    ['StatusContinue', 'StatusSwitchingProtocols']))

range400 = dict(zip([400, 401, 402, 403, 404, 405, 406, 407, 408,
                     409, 410, 411, 412, 413, 414, 415, 416, 417,
                     418],
                    ['StatusBadRequest', 'StatusUnauthorized',
                     'StatusPaymentRequired', 'StatusForbidden',
                     'StatusNotFound', 'StatusMethodNotAllowed',
                     'StatusNotAcceptable', 'StatusProxyAuthRequired',
                     'StatusRequestTimeout', 'StatusConflict',
                     'StatusGone', 'StatusLengthRequired',
                     'StatusPreconditionFailed',
                     'StatusRequestEntityTooLarge',
                     'StatusRequestURITooLong',
                     'StatusUnsupportedMediaType',
                     'StatusRequestedRangeNotSatisfiable',
                     'StatusExpectationFailed', 'StatusTeapot']))

range500 = dict(zip([500, 501, 502, 503, 504, 505],
                    ['StatusInternalServerError', 'StatusNotImplemented',
                     'StatusBadGateway', 'StatusServiceUnavailable',
                     'StatusGatewayTimeout',
                     'StatusHTTPVersionNotSupported']))
