=========================
 edderkopp
=========================

*edderkopp* is a web spider with the following limitations:

#. Will conform to the wishes of `robots.txt`. It crawls by default with a `Crawl-Delay` of 2 seconds.
#. Recognizes `nofollow` hints in `META` and also in specific `A` tags.
#. Does not parse for images in `LINK` tags.
#. Any assets (images/js) hosted on CDNs or subdomains will be ignored.

.. sourcecode:: bash

   e.g. If you crawl http://www.cbc.ca,
        
           http://i.cbc.ca/assets/header.jpg is IGNORED
           http://www.cbc.ca/assets/header.jpg is ACCEPTED

Requirements
============

* Code was developed on an Ubuntu Precise machine.
* Ubuntu core packages that needs to be installed:

#. Python 2.7.x
#. Git
#. Graphiz

.. sourcecode:: bash

    $ apt-get install pkg-config
    $ apt-get install graphviz graphviz-dev

#. virtualenv (I used `1.7.1.2`)

Setup
=====

* Create a virtualenv (We will create `venv` in `/tmp`)

.. sourcecode:: bash

    $ cd /tmp
    $ virtualenv --no-site-packages venv
    $ cd venv
    $ source bin/activate

* Checkout the code

.. sourcecode:: bash

    $ git clone https://pontresina@bitbucket.org/pontresina/edderkopp.git

* change directory to `/tmp/venv/edderkopp`.
* Run:

.. sourcecode:: bash

    pip install -r requirements.txt

* If you want to run the test cases, do this extra step

.. sourcecode:: bash

    pip install -r requirements-dev.txt

* Install the package:

.. sourcecode:: bash

    python setup.py develop

* (Optional) Run all tests:

.. sourcecode:: bash

    $ paver test_all
    ======================== 112 passed, 2 xfailed in 2.01 seconds
     ___  _   ___ ___ ___ ___
    | _ \/_\ / __/ __| __|   \
    |  _/ _ \\__ \__ \ _|| |) |
    |_|/_/ \_\___/___/___|___/

Example
=======

* You should now be in directory:

.. sourcecode:: bash

    $ cd /tmp/venv/edderkopp/edderkopp

* Run the crawler:

.. sourcecode:: bash

    $ ./main.py http://buzz.jaysalvat.com/
    
* It will generate a `dot` file in `/tmp`.
* Then you can convert it with Graphiz to a PNG like this:


.. sourcecode:: bash

    $ dot buzzjaysalvatcom_1402624035.dot -Tpng -o sitemap.png
    
* You can view 2 sample sitemap at http://imgur.com/a/q3h1a